<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://raketech.com
 * @since             1.0.0
 * @package           Rakereviews
 *
 * @wordpress-plugin
 * Plugin Name:       Raketech Reviews
 * Plugin URI:        https://raketech.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Sam Hayman
 * Author URI:        https://raketech.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rakereviews
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Store plugin base dir, for easier access later from other classes.
 * (eg. Include, pubic or admin)
 */
define( 'REVIEWS_BASE_PATH', plugin_dir_path( __FILE__ ) );

/**
 * Here we define the transient names
 * 
 * ARRAY - we cache the original array in its
 * original form, with the exception that we
 * sort it according to 'position' key.
 * 
 * We cache it since it's good practice to reduce API calls
 * And also makes for faster loading times.
 * 
 * ORDER - is where we save the reviews order
 */
define( 'RAKE_REVIEW_ARRAY', 'rake_review_array' );
define( 'RAKE_REVIEW_ORDER', 'rake_review_order' );

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'RAKEREVIEWS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-rakereviews-activator.php
 */
function activate_rakereviews() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-rakereviews-activator.php';
	Rakereviews_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-rakereviews-deactivator.php
 */
function deactivate_rakereviews() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-rakereviews-deactivator.php';
	Rakereviews_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_rakereviews' );
register_deactivation_hook( __FILE__, 'deactivate_rakereviews' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-rakereviews.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_rakereviews() {

	$plugin = new Rakereviews();
	$plugin->run();

}
run_rakereviews();
