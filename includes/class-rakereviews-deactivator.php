<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://raketech.com
 * @since      1.0.0
 *
 * @package    Rakereviews
 * @subpackage Rakereviews/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Rakereviews
 * @subpackage Rakereviews/includes
 * @author     Sam Hayman <hayman.sam@gmail.com>
 */
class Rakereviews_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
