<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://raketech.com
 * @since      1.0.0
 *
 * @package    Rakereviews
 * @subpackage Rakereviews/admin/partials
 */

// autoloader to load all the classes we need
require_once ( REVIEWS_BASE_PATH . 'admin/partials/autoloader.php' );

/**
 * If we're in the Admin area
 * we load the preloader and toast notifications
 * 
 * Just HTML, but keep the code clean
 */
if ( is_admin() ) {
    include 'toaster.php';
    include 'preloader.php';
}

/**
 * We get the JSON data, decode it and turn it into an MultiDimensional Array
 */
$file = RakeReviewsApi::file( REVIEWS_BASE_PATH . 'data.json', RAKE_REVIEW_ARRAY );

/**
 * Once we receive the data back, we traverse the array
 * 
 * We then extract the data in "list-item.php"
 */
$array = RakeReviewsParser::parse( $file, RAKE_REVIEW_ARRAY, RAKE_REVIEW_ORDER );
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="raketech-reviews-plugin-admin-page">
    <div class="raketech-admin-content">
        <div class="raketech-headings">
            <ul class="headings">
                <li class="heading">Casino</li>
                <li class="heading">Bonus</li>
                <li class="heading">Features</li>
                <li class="heading">Play</li>
            </ul>
        </div>
        <ul id="sortable">
            <?php
            /**
             * We keep code clean so we can put the
             * li tag content template in a seperate file
             * and loop it.
             */
            foreach ($array->output as $review) {
                include('list-item.php');
            }
            ?>
        </ul>
    </div>
</div>