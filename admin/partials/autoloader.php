<?php

function _autoloader($class) {
    $class = strtolower($class);
    require_once ( REVIEWS_BASE_PATH . 'admin/partials/classes/class-' . $class . '.php' );
}

spl_autoload_register('_autoloader');
