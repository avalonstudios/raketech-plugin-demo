<?php

class RakeReviewsApi {

    public $file;

    public function __construct( $data )
    {

        $this->file = $data;

    }

    public static function file( $file )
    {

        /**
         * First we get the file contents - 'file_get_contents'
         * then we json_decode it and transform it into an associative array
         * by assigning the second parameter to TRUE
         * 
         *      json_decode($file, TRUE)
         * 
         * In this case, we can immediately traverse down to 'toplists'
         * and then to '575' as required by the specs
         */

        if ( ! get_transient( RAKE_REVIEW_ARRAY ) ) {

            /**
             * In essence, this fires only first time the user visits the plugin page
             * 
             * We could add a button and function to delete this (or all the) transient/s
             * created by the plugin or once the plugin is deactivated or deleted.
             */
            
            return new self( json_decode( file_get_contents( $file ), true )[ 'toplists' ][ '575' ] );

        } else {

            return new self( get_transient( RAKE_REVIEW_ARRAY ) );

        }

    }

}