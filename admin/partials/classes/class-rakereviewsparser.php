<?php

class RakeReviewsParser {
    
    public $output;

    public function __construct( $data )
    {
     
        $this->output = $data;

    }

    static public function parse( $array, $transientArray, $transientOrder )
    {
        
        return new self( self::parser( $array, $transientArray, $transientOrder ) );

    }
    
    static private function parser( $array, $transientArray, $transientOrder )
    {
        /**
         * We instantiate 3 variables
         * 
         * $reviewsArr - will contain the initial array from JSON
         * The first time it loads it will be jumbled up.
         * 
         * $sorted - will hold the order from $transientOrder
         * $i - we use to iterate and add 
         */

        $reviewsArr = [];
        $sorted;
        $i = 0;

        foreach ( $array->file as $value ) {
            $brand_id   = (int) $value[ 'brand_id' ];
            $position   = (int) $value[ 'position' ];
            $rating     = (int) $value[ 'info' ][ 'rating' ];
            $bonus      = ( $value[ 'info' ][ 'bonus' ] );
            $info       = (array) $value[ 'info' ];
            $features   = $info[ 'features' ];
            $tc         = ( $value[ 'terms_and_conditions' ] );
            $logo       = ( $value[ 'logo' ] );
            $play_url   = ( $value[ 'play_url' ] );

            /**
             * Store the $i variable in array '$positions'
             * We'll use this to insert in $transientOrder
             * since the first thing we'll do is sort the raw data
             * according to the position key in the JSON output
             */
            $positions[] = $i;

            /**
             * Create a $sortOrder array to hold the
             * positions in their original order
             */
            $sortOrder[] = $position;

            /**
             * array_splice( INPUT, OFFSET, LENGTH, REPLACEMENT );
             * The function below does the following: 
             * In our $reviewsArr array (INPUT)
             * Put the data from JSON (REPLACEMENT)
             * Into $reviewsArr index at $position (OFFSET)
             * and over/write only that value (OFFSET = 0)
             * 
             * As an example, this will cause the JSON object
             * with position: 2, to be inserted at index 2 in
             * the $reviewsArr array we created
             */
            array_splice(
                $reviewsArr,
                $position, //sortOrder[$i]
                0,
                [
                    [
                        'brand_id'              => $brand_id,
                        'position'              => $position,
                        'info'                  => $info,
                        'terms_and_conditions'  => $tc,
                        'logo'                  => $logo,
                        'play_url'              => $play_url,
                    ]
                ]
            );
            $i++;
        } // endforeach

        if ( ! get_transient( $transientOrder ) ) {
            /**
             * When plugin is first loaded, create two transients
             * 
             * transientOrder - stores the order according to the
             * 'position' key (0, 1, 2, 3, 4...)
             * 
             * transientArray - stores the array itself in the same order
             * 
             * We then give both an expirt of 100 years so
             * it doesn't autoload, thus saving resources
             */
            set_transient( $transientOrder, $positions, 100 * YEAR_IN_SECONDS );
            set_transient( $transientArray, $reviewsArr, 100 * YEAR_IN_SECONDS );
        }

        // Now grab the order from transientOrder
        $sorted = get_transient( $transientOrder );

        /**
         * Instantiate a new array
         * and an iterator (we can reset $i)
         */
        $newArr = [];
        $i = 0;

        /**
         * After we fill the $reviewsArr from above
         * we sort the $newArr according to the order
         * saved in the database stored in '$sorted'.
         * 
         * For e.g., if the order in DB is
         * [1, 3, 0, 2]
         * We iterate through the $reviewsArr array
         * so whatever data is at index 1, will be
         * inserted at index 1 in the $newArr array.
         */
        foreach ( $reviewsArr as $v ) {
            array_splice(
                $newArr,
                $i,
                0,
                [ $reviewsArr[ $sorted[ $i ] ] ]
            );
            $i++;
        }

        return $newArr;

    }

}
