<?php
/**
 * Here we grab specific portions of data and store them in variables
 */
$brandId    = $review[ 'brand_id' ];
$position   = $review[ 'position' ];
$rating     = $review[ 'info' ][ 'rating' ];
$bonus      = $review[ 'info' ][ 'bonus' ];
$features   = $review[ 'info' ][ 'features' ];
$tc         = $review[ 'terms_and_conditions' ];
$logo       = $review[ 'logo' ];
$play_url   = $review[ 'play_url' ];
?>

<?php
/**
 * li#sort-$position will be the ID used for jQuery UI Sortable library
 * which will enable the user to drag&drop the reviews in the desired order
 */
?>
<li class="sortable-item" id="sort-<?= $position; ?>">
    <div class="logo">
        <a href="<?= home_url('/') . $brandId; ?>" rel="bookmark">
            <div class="image">
                <?php
                /**
                 * It's always "best practice" to add 'width' and 'height'
                 * attributes to img tags.
                 * 
                 * getimagesize($img)[3] will output the attributes automatically
                 */
                ?>
                <img src="<?= $logo; ?>" <?= getimagesize($logo)[3]; ?>>
            </div>
            <div class="review">Review</div>
        </a>
    </div>
    <div class="rating-bonus">
        <div class="star-rating rating-<?= round( $rating ); ?>"></div>
        <?php
        /**
         * Adding some accessibility to the star rating through aria-label
         */
        ?>
        <div aria-label="<?= $rating; ?> star"></div>
        <div class="bonus"><?= $bonus; ?></div>
    </div>
    <div class="features">
        <ul class="feature-list">
            <?php foreach ($features as $feature) : ?>
                <?php
                /**
                 * We loop through the features array
                 * and sanitize the output
                 */
                ?>
                <li class="feature-item"><?= wp_kses_post($feature); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="button-terms">
        <a href="<?= $play_url; ?>" class="btn btn-primary" rel="noopener noreferrer">play now</a>
        <div class="terms"><?= $tc; ?></div>
    </div>
</li>
