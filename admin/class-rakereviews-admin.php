<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://raketech.com
 * @since      1.0.0
 *
 * @package    Rakereviews
 * @subpackage Rakereviews/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Rakereviews
 * @subpackage Rakereviews/admin
 * @author     Sam Hayman <hayman.sam@gmail.com>
 */
class Rakereviews_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rakereviews_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rakereviews_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/rakereviews-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rakereviews_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rakereviews_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// enqueue jQuery
		wp_enqueue_script( 'rake-jquery', plugin_dir_url( __FILE__ ) . 'js/jquery/jquery.3.5.1.min.js', null, $this->version, false );
		
		// enqueue jQuery UI
		wp_enqueue_script( 'rake-jqueryui', plugin_dir_url( __FILE__ ) . 'js/jquery-ui/jquery-ui.min.js', null, $this->version, false );

		// enqueue custom JS
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/min/rakereviews-admin.js', [ 'rake-jquery', 'rake-jqueryui' ], $this->version, false );

		wp_localize_script(
			$this->plugin_name,
			'ajaxObj',
			[
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'positions' => get_transient('rake_review_order'),
				'nonce' => wp_create_nonce( 'rake_chocolate_cake' ) // :)
			]
		);

	}

	/**
	 * Callback function to create Reviews Admin menu
	 */
	public function raketech_review_menu() {

		add_menu_page(
			'Reviews Demo Plugin',
			'Reviews Demo',
			'manage_options',
			$this->plugin_name,
			[ $this, 'display_plugin_setup_page' ]
		);

	}

	// add_menu_page callback function
	public function display_plugin_setup_page() {

		require_once( 'partials/' . $this->plugin_name . '-admin-display.php' );

	}

	// AJAX callback function
	public function rake_save_item_order() {

		/**
		 * If we can't verify the nonce
		 * bail out immediately
		 */
		if ( ! wp_verify_nonce( $_POST[ 'nonce' ], 'rake_chocolate_cake' ) ) {
			die();
		}

		$trOrder = 'rake_review_order'; // transient name for array order

		// Get the 'order' variable set through AJAX
		$order = $_POST[ 'order' ];

		/**
		 * extract only the number from ID
		 * 'sort-1' will give us '1' only
		 */
		 $sortOrder = trim( preg_replace( '/[^0-9]+/', ' ', $order ) );

		 /**
		 * Put each number in an array and make sure
		 * it's an integer and not a string ('intval')
		 */
		$splitNum = array_map( 'intval', explode( ' ', $sortOrder ) );

		// set the transient
		$isSaved = set_transient( $trOrder, $splitNum, 100 * YEAR_IN_SECONDS );

		if ( $isSaved ) {
			echo true;
		} else {
			echo false;
		}

		die();

	}

}
