// import the needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { faStar as fasFaStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as farFaStar } from '@fortawesome/free-regular-svg-icons';

library.add(fasFaStar, farFaStar);
dom.watch();

jQuery(($) => {
	var itemList = $('#sortable');
	itemList.sortable({
		axis: 'y',
		update: function (event, ui) {
			$('#loading-animation').show();

			var opts = {
				url: ajaxurl,
				type: 'POST',
				async: true,
				cache: false,
				dataType: 'json',
				data: {
					action: 'item_sort',
					order: itemList.sortable('toArray').toString(), // Pass list items IDs e.g. sort-1, sort-2 etc
					nonce: ajaxObj.nonce,
				},
				success: function (response) {
					$('#loading-animation').fadeOut(); // Hide the loading animation
					$('#toaster .save-success').css('display', 'block').delay(2000).fadeOut();
					return;
				},
				error: function (xhr, textStatus, e) {
					$('#toaster .save-error').css('display', 'block').delay(2000).fadeOut();
					$('#loading-animation').fadeOut(); // Hide the loading animation
					return;
				}
			};
			$.ajax(opts);
		}
	});
});
